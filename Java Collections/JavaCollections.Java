import java.util.*;

public class JavaCollections {
    public static void main(String[] args) {


        System.out.println("=== Lists Allows Duplicates ===");
        List list = new ArrayList();
        list.add("Testing");
        list.add("Out");
        list.add("Java");
        list.add("Collection");
        list.add("List");
        list.add("Sets");
        list.add("Testing");

        System.out.println(list);

        for (Object str : list) {
            System.out.println((String)str);
        }


        // Sets are unique lists. Two kinds of sets are HashSet and TreeSet. TreeSet will also sort
        System.out.println("=== Sets - Unique Lists with Sorted (Tree) and Unsorted (Hash) ===");
        Set set = new TreeSet();
        set.add("Testing");
        set.add("Out");
        set.add("Java");
        set.add("Collection");
        set.add("List");
        set.add("Sets");
        set.add("Testing");


        System.out.println(set);

        for (Object str : set) {
            System.out.println((String)str);
        }

        System.out.println("=== Queue - First In First Out (FIFO) ===");
        Queue queue =  new PriorityQueue();
        queue.add("Testing");
        queue.add("Out");
        queue.add("Java");
        queue.add("Collection");
        queue.add("List");
        queue.add("Sets");
        queue.add("Testing");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }


        System.out.println("========== Deque with LinkedList==========");
        Deque deque = new LinkedList();
        deque.add("1 - Tail");
        deque.add("2 - Head");
        deque.add("3 - Tail");
        deque.add("4 - Head");
        deque.add("5 - Tail");
        deque.add("6 - Head");
        deque.add("7 - Tail");

        System.out.println(deque + "\n");
        System.out.println("Standard Loop");
        Iterator dQiterator = deque.iterator();
        while (dQiterator.hasNext()) {
            System.out.println("\t" + dQiterator.next());
        }

        System.out.println("Reverse Loop");
        Iterator reverse = deque.descendingIterator();
        while (reverse.hasNext()) {
            System.out.println("\t" + reverse.next());
        }

        System.out.println("Peek - See head of queue without deleting");
        System.out.println("Peek " + deque.peek());
        System.out.println("After Peek: " + deque);

        System.out.println("Pop - See head of queue and delete");
        System.out.println("Pop " + deque.pop());
        System.out.println("After Pop: " + deque);



        System.out.println("=== Map - Key/Pair Values ===");
        Map map = new HashMap();
        map.put(1,"Testing");
        map.put(2,"Out");
        map.put(3,"Java");
        map.put(4,"Collection");
        map.put(5,"List");
        map.put(6,"Sets");
        map.put(5,"Testing");

        for (int i = 1; i < 7; i++) {
            String result = (String)map.get(i);
            System.out.println(result);
        }




    }
}

