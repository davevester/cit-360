package ServletExample;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;


@WebServlet(name = "process", urlPatterns = {"/process"})
public class process extends HttpServlet {

    HashMap<String, HashMap<String, String>> tickets;

    public void init() throws ServletException {

        tickets = new HashMap<String, HashMap<String, String>>();
        tickets.put("1", new HashMap<String, String>());

        tickets.get("1").put("name"  , "Bob Ross");
        tickets.get("1").put("email" , "bob@ross.com");
        tickets.get("1").put("phone" , "123.234.3456");
        tickets.get("1").put("issue" , "Can't Login.");
        tickets.get("1").put("detail" , "User reset password and now is unable to login.");

        tickets.put("2", new HashMap<String, String>());

        tickets.get("2").put("name"  , "Lavar Burton");
        tickets.get("2").put("email" , "lavar@readingrainbow.com");
        tickets.get("2").put("phone" , "555.444.3211");
        tickets.get("2").put("issue" , "Computer won't turn on.");
        tickets.get("2").put("detail" , "Power outage in building and now the computer won't turn on.");



    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String login = request.getParameter("login");
        String password = request.getParameter( "password");
        out.println("<h1>Login Information</h1>");
        out.println("<p>Login: " + login + "</p>");
        out.println("<p>Password: " + password + "</p>");
        out.println("</body></html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("<html><head></head><body>");
        String ticketID = request.getParameter("id");
        int tID = Integer.parseInt(ticketID);

        if (tID == 0) {
            out.println("<h2>Ticket Queue</h2>");
            out.println("<table style=\"text-align:left\"><tr><th>Ticket No.</th><th>Name</th><th>Email</th><th>Phone</th><th>Issue</th></tr>");
            for (String i : tickets.keySet()) {
                out.println("<tr><td><a href=\"process?id=" + i + "\" >Details</a></td><td>" + tickets.get(i).get("name") + "</td><td>" + tickets.get(i).get("email") + "</td><td>" + tickets.get(i).get("phone") + "</td><td>" + tickets.get(i).get("issue") + "</td></tr>");

            }
            out.println("</table>");
        } else {
            out.println("<h2>Ticket Info for #" + ticketID + "</h2>");
            out.println("<table style=\"text-align:left\"><tr><th>Name</th><th>Email</th><th>Phone</th><th>Issue</th></tr>");
            out.println("<tr><td>" + tickets.get(ticketID).get("name") + "</td><td>" + tickets.get(ticketID).get("email") + "</td><td>" + tickets.get(ticketID).get("phone") + "</td><td>" + tickets.get(ticketID).get("issue") + "</td></tr>");
            out.println("</table>");
            out.println("<p>Detail:</p>");
            out.println(tickets.get(ticketID).get("detail"));
            out.println("<p><a href=\"process?id=0\">Back to Queue</a></p>");
        }

        out.println("</body></html>");


    }
}