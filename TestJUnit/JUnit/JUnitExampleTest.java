package JUnit;

import org.junit.Test;

import static org.junit.Assert.*;

public class JUnitExampleTest {

    @Test
    public void nameTest() throws Exception {
        JUnitExample example1 = new JUnitExample();
        example1.setName("David");
        assertEquals("David", example1.getName());
    }

    @Test
    public void squareTest() {
        JUnitExample example2 = new JUnitExample();
        int result = example2.square(5);
        assertEquals(25, result);

    }

    @Test
    public void letterTest() {
        JUnitExample example3 = new JUnitExample();
        int letterResult = example3.letterCount("alpha");
        assertEquals(2, letterResult);

    }
}