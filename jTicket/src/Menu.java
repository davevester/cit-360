import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Menu {
    boolean exit = false;

    public void printHeader() {
        System.out.println("+----------------------------------+");
        System.out.println("|        Welcome to jTicket        |");
        System.out.println("|      Help Desk Ticket Queue      |");
        System.out.println("|              System              |");
        System.out.println("+----------------------------------+");
    }
    public int LoginMenu(){
        int choice = -1;
        int loggedIn = -1;
        int loginAttempts = 0;
        String userPass = "";


        Scanner kb = new Scanner(System.in);
        while(!exit) {

            System.out.println("Login Menu - Please choose a user: ");


            int usersCount = UsersDao.usersCount();


            while(choice < 0 || choice > usersCount){
                try {
                    UsersDao.displayUsers();
                    System.out.println("0) - Exit");
                    System.out.println("\nEnter your choice: ");
                    choice = Integer.parseInt(kb.nextLine());
                    if (choice == 0) {
                        System.out.println("Exiting system - Goodbye!");
                        System.exit(0);

                    }

                }
                catch (NumberFormatException e) {
                    System.out.println("Invalid Selection. Please try again.");
                }
            }

            while(loggedIn < 0 && loginAttempts < 5){
                try {
                    System.out.println("\nEnter password: ");
                    userPass = kb.nextLine();
                    //System.out.println("You entered: "+userPass);
                    String pword = UsersDao.getPasswordById(choice);
                    //System.out.println("pword var:"+pword);
                    if (!userPass.equals(pword)){
                        System.out.println("Invalid password, please try again");
                        loginAttempts = loginAttempts + 1;
                        if (loginAttempts == 5) {
                            System.out.println("Too many retries - Goodbye!");
                            System.exit(0);
                        }
                    } else {
                        System.out.println("Correct password!");
                        loggedIn = 1;
                    }


                }
                catch (NumberFormatException e) {
                    System.out.println("Invalid Selection. Please try again.");
                }
            }
            return choice;
        }

        return choice;
    }


// Agent Menu
    public int AgentMenu(int id) {

        System.out.println("+----------------------------------+");
        System.out.println("Agent Menu - Please choose an option: ");
        while (!exit) {
            AgentChoice();
            int choice = getAgentInput();
            runAgentAction(choice, id);
        }
        return id;
    }

    public void AgentChoice() {

        System.out.println("\n\n1) View Your Assigned Tickets ");
        System.out.println("2) View Unassigned Tickets ");
        System.out.println("3) View All Resolved Tickets ");
        System.out.println("4) Claim Ticket ");
        System.out.println("5) Close Ticket With Resolution");
        System.out.println("0) Exit ");
    }

    private int getAgentInput() {
        Scanner input = new Scanner(System.in);
        int choice = -1;
        while (choice < 0 || choice > 6) {
            try {
                System.out.println("\nEnter your choice: ");
                choice = Integer.parseInt(input.nextLine());

            }
            catch (NumberFormatException e) {
                System.out.println("Invalid Selection. Please try again. ");
            }

        }
        return choice;
    }

    private void runAgentAction(int choice, int id){
        switch(choice) {
            case 0:
                exit = true;
                System.out.println("Exiting system - Goodbye!");
                break;
            case 1:
                UsersDao.displayActiveTicketsByUser(id);
                break;
            case 2:
                UsersDao.displayUnassignedTickets();
                break;
            case 3:
                UsersDao.displayResolvedTickets();
                break;
            case 4:
                int agentClaimTicket = 0;
                try {
                    Scanner input = new Scanner(System.in);
                    System.out.println("Ticket ID to Claim? ");
                    agentClaimTicket = Integer.parseInt(input.nextLine());
                    UsersDao.claimTicketById(agentClaimTicket, id);
                } catch (NumberFormatException e) {
                    System.out.println("Invalid Selection. Please try again. ");
                }
                UsersDao.claimTicketById(agentClaimTicket, id);
                break;
            case 5:
                int ticketID = 0;
                String resolution = null;
                try {
                    Scanner input = new Scanner(System.in);
                    System.out.println("Which ticket would you like to close? ");
                    ticketID = Integer.parseInt(input.nextLine());
                    System.out.println("What was the resolution? ");
                    resolution = input.nextLine();


                } catch (NumberFormatException e) {
                    System.out.println("Invalid Selection. Please try again. ");
                }
                UsersDao.closeTicketById(ticketID, resolution,id);
                break;
            default:
                System.out.println("Invalid Selection");

        }
    }

// Client Menu
    public int ClientMenu(int id) {
        System.out.println("+----------------------------------+");
        System.out.println("Client Menu - Please choose an option: ");
        while (!exit) {
            ClientChoice();
            int choice = getClientInput();
            runClientAction(choice, id);
        }
        return id;
    }

    public void ClientChoice() {

        System.out.println("\n\n1) View Open Tickets ");
        System.out.println("2) Create New Ticket ");
        System.out.println("3) Close Ticket ");
        System.out.println("0) Exit ");
    }
    private int getClientInput() {
        Scanner input = new Scanner(System.in);
        int choice = -1;
        while (choice < 0 || choice > 5) {
            try {
                System.out.println("\nEnter your choice: ");
                choice = Integer.parseInt(input.nextLine());

            }
            catch (NumberFormatException e) {
                System.out.println("Invalid Selection. Please try again. ");
            }

        }
        return choice;
    }

    private void runClientAction(int choice, int id){
        switch(choice) {
            case 0:
                exit = true;
                System.out.println("Exiting system - Goodbye!");
                break;
            case 1:
                UsersDao.displayActiveTicketsByClient(id);
                break;
            case 2:
                Tickets ticket = new Tickets();
                try {
                    InputStreamReader isr=new InputStreamReader(System.in);
                    BufferedReader br=new BufferedReader(isr);
                    System.out.println("What is your issue? ");
                    String issue = br.readLine();
                    ticket.setIssue(issue);
                    ticket.setClientId(id);
                    System.out.println("Ticket is created! ");
                } catch (IOException ioe) {
                    System.out.println("IO exception raised");
                }
                UsersDao.createTicket(ticket);
                break;
            case 3:
                try {
                    InputStreamReader isr=new InputStreamReader(System.in);
                    BufferedReader br=new BufferedReader(isr);
                    System.out.println("Ticket ID to Close? ");
                    int tID = Integer.parseInt(br.readLine());
                    System.out.println("What was the resolution? ");
                    String issue = br.readLine();
                    UsersDao.closeTicketById(tID,issue,id);
                } catch (IOException ioe) {
                    System.out.println("IO exception raised");
                }

                break;
            case 4:

                break;
            default:
                System.out.println("Invalid Selection");

        }
    }


}
