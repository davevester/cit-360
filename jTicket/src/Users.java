import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="USERS")
public class Users implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="USER_ID", unique = true, nullable = false)
    private Long userID;

    @Column(name="name")
    private String name;

    @Column(name="username")
    private String username;

    @Column(name="password")
    private String password;

    @Column(name="CREATED_ON")
    private Date createdOn;

    @Column(name="enabled", nullable = true)
    private boolean enabled;

    @Column(name="role")
    private int role;

    @Column(name="company")
    private String company;

    @ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinTable(
            name = "USERS_TICKETS",
            joinColumns = { @JoinColumn(name = "USER_ID") },
            inverseJoinColumns = { @JoinColumn(name = "TICKET_ID") }
    )
    private List<Tickets> userTicketList;

    public Long getUserId() {
        return userID;
    }

    public void setUserId(Long userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;

    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public List<Tickets> getUserTicketList() {
        return userTicketList;
    }

    public void setUserTicketList(List<Tickets> userTicketList) {
        this.userTicketList = userTicketList;
    }

    @Override
    public String toString() {

        String resp = this.userID+" | "+this.name+" | "+this.username+" | "+this.password+" | "+this.createdOn;

        return resp;
    }
}