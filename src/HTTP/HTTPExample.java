package HTTP;

import java.io.*;
import java.net.*;
import java.util.*;


public class HTTPExample {
    public static String getHttpContent(String string) {

        String rawHTML = null;


        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder sb = new StringBuilder();

            String line;
            while ((line= reader.readLine()) != null) {
                sb.append(line);
            }
            rawHTML = sb.toString();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }

        return rawHTML;
    }


    public static Map getHttpHeaders(String string) {
        Map hashmap = null;

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            hashmap = http.getHeaderFields();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return hashmap;

    }

    public static void main(String[] args) {

        String website = "http://www.milliondollarhomepage.com";

        System.out.println("Raw HTML pull with getHttpContent for " + website + ":");
        System.out.println(HTTPExample.getHttpContent(website));

        Map<Integer, List<String>> m = HTTPExample.getHttpHeaders(website);

        System.out.println("\nHTTP Headers from map list for " + website + ":");
        for (Map.Entry<Integer,List<String>> entry : m.entrySet()) {
            try {
                System.out.println(entry.getKey() + " : " + entry.getValue());

            } catch(Exception e) {
                System.err.println(e.toString());
            }
        }
    }
}