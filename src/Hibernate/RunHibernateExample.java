package Hibernate;

import java.util.*;

public class RunHibernateExample {

    public static void main(String[] args) {

        managePeople m = managePeople.getInstance();

        List<People> p = m.getPeople();
        for (People i : p) {
            System.out.println(i);
        }

        System.out.println(m.getPerson(1));
        System.out.println(m.addPerson("Luke","Skywalker","luke.skywalker@people.com"));
        System.out.println(m.deletePerson(7));
    }
}
