package JSON;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;


public class JSONExample {

    public static String technicianToJSON(Technician technician) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(technician);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

        public static Technician JSONToTechnician(String s) {
            ObjectMapper mapper = new ObjectMapper();
            Technician technician = null;

                try {
                    technician = mapper.readValue(s, Technician.class);
                } catch (JsonProcessingException e) {
                    System.err.println(e.toString());
                }


            return technician;
        }

    public static void main(String[] args) {
        Technician tech = new Technician();
        tech.setName("David Vester");
        tech.setEmail("davevester@byui.edu");

        String json = JSONExample.technicianToJSON(tech);
        System.out.println(json);

        Technician tech2 = JSONExample.JSONToTechnician(json);
        System.out.println(tech2);
    }

}
