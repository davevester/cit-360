package JUnit;

import org.junit.*;

public class JUnitExample {
    private String name;

    public JUnitExample() {

    }

    public String setName(String name) {
        this.name = name;
        return this.name;
    }
    public String getName() {
        return this.name;
    }

    public int square(int x){
        return x*x;
    }

    public int letterCount(String word){
        int count = 0;
        word = word.toLowerCase();

        for (int i = 0; i < word.length(); i++){
            if(word.charAt(i)== 'a'){
                count++;
            }
        }
        return count;
    }

}
