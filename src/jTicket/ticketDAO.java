package jTicket;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class ticketDAO {

    SessionFactory factory = null;
    Session session = null;

    private static ticketDAO single_instance = null;

    private ticketDAO() {
        factory = HibernateUtils.getSessionFactory();
    }

    public static ticketDAO getInstance() {
        if (single_instance == null) {
            single_instance = new ticketDAO();
        }
        return single_instance;
    }

    public List<Tickets> getPeople() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Hibernate.People";
            List<Tickets> ppl = (List<Tickets>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return ppl;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();

        }
    }

    public Tickets getPerson(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Hibernate.People where id=" + Integer.toString(id);
            Tickets p = (Tickets)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return p;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();

        }
    }

    public Tickets addPerson(String fname, String lname, String email) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();

            Tickets p = new Tickets();
            p.setFirstName(fname);
            p.setLastName(lname);
            p.setEmail(email);

            session.save(p);
            session.getTransaction().commit();
            return p;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();

        }
    }

    public Tickets deletePerson(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Hibernate.People where id=" + Integer.toString(id);
            Tickets p = (Tickets)session.get(Tickets.class, id);
            session.delete(p);
            session.getTransaction().commit();
            return p;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();

        }
    }

}