import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="TICKETS")
public class Tickets {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="TICKET_ID", unique = true, nullable = false)
    private int ticketId;

    @Column(name="issue", nullable = false)
    private String issue;

    @Column(name="resolution", nullable = true)
    private String resolution;

    @Column(name="resolved")
    private boolean resolved;

    @Column(name="CREATED_ON")
    private Date createdOn;

    @Column(name="AGENT_ID")
    private int agentID;

    @Column(name="CLIENT_ID")
    private int clientID;

    @Column(name="CLOSED_BY")
    private int closedBy;



    @ManyToMany(mappedBy = "userTicketList")
    private List<Users> users;

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public boolean getResolved() {
        return resolved;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    public int getAgentId() {
        return agentID;
    }

    public void setAgentId(int agentID) {
        this.agentID = agentID;
    }

    public int getClientId() {
        return clientID;
    }

    public void setClientId(int clientID) {
        this.clientID = clientID;
    }

    public int getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(int closedBy) {
        this.closedBy = closedBy;
    }

    @Override
    public String toString() {

        return this.ticketId +" | "+ this.issue +" | "+ this.resolution;
    }
}