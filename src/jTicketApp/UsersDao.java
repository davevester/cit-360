import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import sun.security.krb5.internal.Ticket;

public class UsersDao {


    public List<Users> getUsersList(){

        Session session = null;
        List<Users> usersList = null;
        try {
            session = HibernateUtil.getSession();
            String queryStr = "select users from Users users";
            Query query = session.createQuery(queryStr);
            //query.setMaxResults(1);
            usersList = query.list();
        } catch(Exception ex) {
            ex.printStackTrace();
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }
        return usersList;
    }

    public static List<Tickets> getTicketsList(){

        Session session = null;
        List<Tickets> ticketList = null;
        try {
            session = HibernateUtil.getSession();
            String queryStr = "select tickets from Tickets tickets";
            Query query = session.createQuery(queryStr);
            //query.setMaxResults(1);
            ticketList = query.list();
        } catch(Exception ex) {
            ex.printStackTrace();
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }
        return ticketList;
    }

    public static List<Tickets> getActiveTicketsById(int id){

        Session session = null;
        List<Tickets> ticketList = null;
        try {
            session = HibernateUtil.getSession();
            String queryStr = "select tickets from Tickets tickets where resolved = 0 and agent_id = "+id;
            Query query = session.createQuery(queryStr);
            //query.setMaxResults(1);
            ticketList = query.list();
        } catch(Exception ex) {
            ex.printStackTrace();
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }
        return ticketList;
    }

    public static List<Tickets> getActiveClientTicketsById(int id){

        Session session = null;
        List<Tickets> ticketList = null;
        try {
            session = HibernateUtil.getSession();
            String queryStr = "select tickets from Tickets tickets where resolved = 0 and client_id = "+id;
            Query query = session.createQuery(queryStr);
            //query.setMaxResults(1);
            ticketList = query.list();
        } catch(Exception ex) {
            ex.printStackTrace();
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }
        return ticketList;
    }

    public static List<Tickets> getUnassignedTickets(){

        Session session = null;
        List<Tickets> ticketList = null;
        try {
            session = HibernateUtil.getSession();
            String queryStr = "select tickets from Tickets tickets where resolved = 0 and agent_id = 0";
            Query query = session.createQuery(queryStr);
            //query.setMaxResults(1);
            ticketList = query.list();
        } catch(Exception ex) {
            ex.printStackTrace();
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }
        return ticketList;
    }

    public static List<Tickets> getResolvedTickets(){

        Session session = null;
        List<Tickets> ticketList = null;
        try {
            session = HibernateUtil.getSession();
            String queryStr = "select tickets from Tickets tickets where resolved = 1";
            Query query = session.createQuery(queryStr);
            //query.setMaxResults(1);
            ticketList = query.list();
        } catch(Exception ex) {
            ex.printStackTrace();
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }
        return ticketList;
    }




    public static String getUsernameById(int id){

        Session session = null;
        String username = null;
        try {
            session = HibernateUtil.getSession();
            String queryStr = "select username from Users users where user_id=" + id;
            Query query = session.createQuery(queryStr);
            //query.setMaxResults(1);
            username = (String) query.getSingleResult();
        } catch(Exception ex) {
            ex.printStackTrace();
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }
        return username;
    }

    public static String getPasswordById(int id){

        Session session = null;
        String password = null;
        try {
            session = HibernateUtil.getSession();
            String queryStr = "select password from Users users where user_id=" + id;
            Query query = session.createQuery(queryStr);
            //query.setMaxResults(1);
            password = (String) query.getSingleResult();
        } catch(Exception ex) {
            ex.printStackTrace();
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }
        return password;
    }

    public static int getRoleById(int id){

        Session session = null;
        int role = 0;
        try {
            session = HibernateUtil.getSession();
            String queryStr = "select role from Users users where user_id=" + id;
            Query query = session.createQuery(queryStr);
            //query.setMaxResults(1);
            role = (int) query.getSingleResult();
        } catch(Exception ex) {
            ex.printStackTrace();
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }
        return role;
    }

    public static void displayTickets() {

        UsersDao usersDao = new UsersDao();
        List<Tickets> ticketsList = getTicketsList();

        System.out.println("All Tickets:");

        ticketsList.forEach(e -> {
            System.out.println("ID: " + e.getTicketId() + " - Issue: " + e.getIssue());


            //e.getTicketId().stream().forEach(System.out::println);
            //System.out.println("---------------------------");
        });

    }

    public static void displayUnassignedTickets() {

        UsersDao usersDao = new UsersDao();
        List<Tickets> ticketsList = getUnassignedTickets();

        System.out.println("Unassigned Tickets:");

        ticketsList.forEach(e -> {
            System.out.println("ID: " + e.getTicketId() + " - Issue: " + e.getIssue());


            //e.getTicketId().stream().forEach(System.out::println);
            //System.out.println("---------------------------");
        });

    }

    public static void displayResolvedTickets() {

        UsersDao usersDao = new UsersDao();
        List<Tickets> ticketsList = getResolvedTickets();

        System.out.println("Resolved Tickets:");

        ticketsList.forEach(e -> {
            System.out.println("ID: " + e.getTicketId() + " - Issue: " + e.getIssue());


            //e.getTicketId().stream().forEach(System.out::println);
            //System.out.println("---------------------------");
        });

    }

        public static void displayActiveTicketsByUser(int id){

            UsersDao usersDao = new UsersDao();
            List<Tickets> ticketsList = getActiveTicketsById(id);

            System.out.println("Your Active Tickets:");

            ticketsList.forEach(e -> {
                System.out.println("Ticket ID: "+e.getTicketId()+ " - Issue: " +e.getIssue());



                //e.getTicketId().stream().forEach(System.out::println);
                //System.out.println("---------------------------");
            });








    }
    public static void displayUsers(){

        UsersDao usersDao = new UsersDao();
        List<Users> usersList = usersDao.getUsersList();

        usersList.stream().forEach(e -> {
            //System.out.println(e);
            System.out.println(e.getUserId() + ") - " + e.getName());
            //System.out.println("\n-- projects assigned to " + e.getName() + " --");
            //e.getEmpAssignmentList().stream().forEach(System.out::println);
            //System.out.println("---------------------------");
        });


    }

    public static void displayActiveTicketsByClient(int id) {

        UsersDao usersDao = new UsersDao();
        List<Tickets> ticketsList = getActiveClientTicketsById(id);

        System.out.println("Your Active Tickets:");

        ticketsList.forEach(e -> {
            System.out.println("Ticket ID: " + e.getTicketId() + " - Issue: " + e.getIssue());


            //e.getTicketId().stream().forEach(System.out::println);
            //System.out.println("---------------------------");
        });
    }
    public static int usersCount(){

        UsersDao usersDao = new UsersDao();
        List<Users> usersList = usersDao.getUsersList();

        return usersList.size();

    }


    public static int checkPword(int uID, String password){

        Session session = null;
        List usersList = null;
        try {
            session = HibernateUtil.getSession();
            String queryStr = "select users from Users users where USER_ID = " + Integer.toString(uID);
            Query query = session.createQuery(queryStr);
            usersList = query.list();

        } catch(Exception ex) {
            ex.printStackTrace();
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }

        return usersList.size();

    }

    public static Tickets getTicketById(int id){

        Session session = null;
        Tickets ticket = null;
        try {
            session = HibernateUtil.getSession();
            String queryStr = "select tickets from Tickets tickets where ticket_id = "+id;
            Query query = session.createQuery(queryStr);
            //query.setMaxResults(1);
            ticket = (Tickets) query.getSingleResult();
        } catch(Exception ex) {
            ex.printStackTrace();
            System.out.println("Ticket does not exists");
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }
        return ticket;
    }

    public static void deleteTicketById(int id){

        Session session = null;

        try {
            session = HibernateUtil.getSession();
            session.getTransaction().begin();
            Tickets ticket = session.get(Tickets.class, id);
            if (ticket != null) {
                session.delete(ticket);
                System.out.println("Ticket has been deleted");
            }
                session.getTransaction().commit();

        } catch(Exception ex) {
            ex.printStackTrace();
            session.getTransaction().rollback();
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }
    }

    public static void claimTicketById(int id, int aID){

        Session session = null;

        try {
            session = HibernateUtil.getSession();
            session.getTransaction().begin();
            Tickets ticket = session.get(Tickets.class, id);
            if (ticket.getAgentId() >= 1) {
                System.out.println("Ticket is already claimed by another agent");
            } else {
                ticket.setAgentId(aID);
                session.update(ticket);
                session.getTransaction().commit();
                System.out.println("Ticket has been claimed");

            }

        } catch(Exception ex) {
            ex.printStackTrace();
            session.getTransaction().rollback();
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }
    }

    public static void closeTicketById(int id, String resolution, int closedBy){

        Session session = null;

        try {
            session = HibernateUtil.getSession();
            session.getTransaction().begin();
            Tickets ticket = session.get(Tickets.class, id);

            ticket.setResolution(resolution);
            ticket.setResolved(true);
            ticket.setClosedBy(closedBy);

            session.update(ticket);
            session.getTransaction().commit();
            System.out.println("Ticket has been closed");

        } catch(Exception ex) {
            ex.printStackTrace();
            session.getTransaction().rollback();
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }
    }

    public static void createTicket(Tickets t){

        Session session = null;

        try {
            session = HibernateUtil.getSession();
            session.getTransaction().begin();
            session.save(t);
            session.getTransaction().commit();

        } catch(Exception ex) {
            ex.printStackTrace();
            // handle exception here
        } finally {
            try {if(session != null) session.close();} catch(Exception ex) {}
        }
    }


}