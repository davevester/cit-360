import java.util.List;

public class jTicketApp {


    public static void main(String[] args) {

        Menu menu = new Menu();
        menu.printHeader();
        int uID = menu.LoginMenu();
        int role = UsersDao.getRoleById(uID);

        if (role == 1) { // is a ticket agent in the system
        menu.AgentMenu(uID);
        } else if (role == 2) { // is a client in the system
            menu.ClientMenu(uID);
       }



    }

}
